package pro.devapp.btag;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.databinding.DataBindingUtil;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;

import pro.devapp.btag.components.Constants;
import pro.devapp.btag.databinding.ActivityDeviceDashboardBinding;
import pro.devapp.btag.models.DeviceModel;
import pro.devapp.btag.services.BLEService;

public class DeviceDashboardActivity extends AppCompatActivity {

    private ActivityDeviceDashboardBinding binding;
    private DeviceModel model;
    private BLEService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_device_dashboard);
        getSupportActionBar().setTitle("Просмотр");

        if(getIntent().getExtras() != null && getIntent().getStringExtra("myjson") != null){
            try{
                Gson gson = new Gson();
                model = gson.fromJson(getIntent().getStringExtra("myjson"), DeviceModel.class);
                binding.deviceName.setText(model.getName());
                binding.deviceAddress.setText(model.getId());
            } catch (Exception e){
                Log.d("Err", e.getMessage());
            }
        }

        binding.connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(service!=null){
                    service.connect(model.getId());
                }
            }
        });

        binding.disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(service!=null){
                    service.disconnect(model.getId());
                }
            }
        });

        binding.highAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(service!=null){
                    service.immediateAlert(model.getId(), Constants.HIGH_ALERT );
                }
            }
        });

        binding.noAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(service!=null){
                    service.immediateAlert(model.getId(), Constants.NO_ALERT);
                }
            }
        });

        binding.enableLossAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                service.setLinkLossNotificationLevel(model.getId(), Constants.HIGH_ALERT );
            }
        });

        binding.disableLossAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                service.setLinkLossNotificationLevel(model.getId(), Constants.NO_ALERT);
            }
        });

        binding.readAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                service.readImmediateAlert(model.getId());
            }
        });

        binding.readLost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                service.readEnablePeerDeviceNotifyMe(model.getId());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // bind service
        bindService(new Intent(this, BLEService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(serviceConnection);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder instanceof BLEService.BackgroundBluetoothLEBinder) {
                service = ((BLEService.BackgroundBluetoothLEBinder) iBinder).service();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(BLEService.TAG, "onServiceDisconnected()");
        }
    };
}
