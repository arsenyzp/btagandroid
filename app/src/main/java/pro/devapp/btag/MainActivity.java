package pro.devapp.btag;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pro.devapp.btag.adapters.ConnectedDevicesAdapter;
import pro.devapp.btag.components.RecyclerItemClickListener;
import pro.devapp.btag.databinding.ActivityMainBinding;
import pro.devapp.btag.models.DeviceModel;
import pro.devapp.btag.services.BLEService;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private final int PERMISSION_REQUEST_CODE = 111;
    private LinearLayoutManager layoutManager;
    private ConnectedDevicesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        getSupportActionBar().setTitle("Мои устройства");

        adapter = new ConnectedDevicesAdapter(this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.list.setHasFixedSize(true);
        binding.list.setLayoutManager(layoutManager);
        binding.list.setItemAnimator(new DefaultItemAnimator());
        binding.list.setItemViewCacheSize(30);
        binding.list.setDrawingCacheEnabled(true);
        binding.list.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        binding.list.setAdapter(adapter);
        binding.list.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                /**
                 * View item
                 */
                DeviceModel model = adapter.getItem(position);
                Intent intent = new Intent(MainActivity.this, DeviceDashboardActivity.class);
                Gson gson = new Gson();
                String myjson = gson.toJson(model);
                intent.putExtra("myjson", myjson);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));


        List<DeviceModel> list = new ArrayList<>();
        DeviceModel model1 = new DeviceModel();
        model1.setId("werre");
        model1.setName("ewfwf");
        list.add(model1);
        DeviceModel model2 = new DeviceModel();
        model2.setId("werre");
        model2.setName("ewfwf");
        list.add(model2);
        adapter.addItems(list, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_device_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_open_scan:
                Intent intent = new Intent(MainActivity.this, ScanDeviceActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED){
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                    Toast.makeText(this, "The permission to get BLE location data is required", Toast.LENGTH_SHORT).show();
                }else{
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                }
                return;
            }
        }
        runService();
    }

    private void runService() {
        if(!isMyServiceRunning(BLEService.class)){
            Thread t = new Thread("StartSocket") {
                public void run() {
                    Intent serviceIntent = new Intent(getBaseContext(), BLEService.class);
                    getBaseContext().startService(serviceIntent);
                }
            };
            t.start();
        } else {
            Toast.makeText(this, "Сервис уже запущен", Toast.LENGTH_SHORT).show();
        }
    }

    private void showSuccessPermissionGranted() {
        Toast.makeText(this, "Разрешения получены", Toast.LENGTH_SHORT).show();
    }

    private void showSuccessPermissionNotGranted() {
        Toast.makeText(this, "Без разрешений работа приложения не возможна", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 2) {
            showSuccessPermissionGranted();
            runService();
        } else {
            showSuccessPermissionNotGranted();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Проверка запуска сервиса
     * @param serviceClass
     * @return
     */
    protected boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
