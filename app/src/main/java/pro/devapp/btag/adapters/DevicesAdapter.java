package pro.devapp.btag.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pro.devapp.btag.R;
import pro.devapp.btag.databinding.ItemDeviceBinding;
import pro.devapp.btag.modelViews.DeviceModelView;
import pro.devapp.btag.models.DeviceModel;

/**
 * Created by devapp on 30/11/2017.
 */

public class DevicesAdapter  extends RecyclerView.Adapter<DevicesAdapter.ViewHolder> {

    private Context context;
    private List<DeviceModel> items = new ArrayList<>();

    public DevicesAdapter(Context context){
        this.context = context;
    }

    public void addItems(List<DeviceModel> items, boolean reset){
        if(reset){
            notifyItemRangeRemoved(0, this.items.size());
            this.items.clear();
        }
        this.items.addAll(items);
        if(reset){
            notifyDataSetChanged();
        } else {
            notifyItemRangeInserted(this.items.size()-items.size(), this.items.size());
        }
    }

    public void addItem(DeviceModel item){
        if(!this.items.contains(item)){
            this.items.add(item);
            notifyItemRangeInserted(this.items.size(), 1);
        }
    }

    @Override
    public DevicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =  LayoutInflater.from(this.context);
        ItemDeviceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_device, parent, false);

        // set the view's size, margins, paddings and layout parameters
        DevicesAdapter.ViewHolder vh = new DevicesAdapter.ViewHolder(binding.getRoot());
        vh.binding = binding;
        return vh;
    }

    @Override
    public void onBindViewHolder(DevicesAdapter.ViewHolder holder, int position) {
        DeviceModel model = items.get(position);
        holder.binding.setItem(new DeviceModelView(model));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public DeviceModel getItem(int position){
        return items.get(position);
    }

    public void clear() {
        this.items.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ItemDeviceBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
