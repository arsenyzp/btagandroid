package pro.devapp.btag.components;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by devapp on 30/11/2017.
 */

public class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
    private OnItemClickListener mListener;
    private GestureDetector mGestureDetector;

    private View childView;
    private int position;


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
        public void onItemLongClick(View view, int position);
    }

    public RecyclerItemClickListener(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e){
                Log.d(RecyclerItemClickListener.class.getName(), "onDoubleTap");
                mListener.onItemLongClick(childView, position);
                return true;
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                Log.d(RecyclerItemClickListener.class.getName(), "onSingleTapUp");
                mListener.onItemClick(childView, position);
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                Log.d(RecyclerItemClickListener.class.getName(), "onDoubleTap");
                mListener.onItemLongClick(childView, position);
            }


        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        childView = view.findChildViewUnder(e.getX(), e.getY());
        position = view.getChildAdapterPosition(childView);
        if (childView != null && mListener != null) {
            mGestureDetector.onTouchEvent(e);
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
