package pro.devapp.btag.modelViews;

import pro.devapp.btag.models.DeviceModel;

/**
 * Created by devapp on 30/11/2017.
 */

public class DeviceModelView {
    private DeviceModel model;

    public DeviceModelView(DeviceModel model){
        this.model = model;
    }

    public String getName() {
        return this.model.getName();
    }

    public String getId() {
        return this.model.getId();
    }
}
