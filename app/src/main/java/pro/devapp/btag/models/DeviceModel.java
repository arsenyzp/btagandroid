package pro.devapp.btag.models;

/**
 * Created by devapp on 30/11/2017.
 */

public class DeviceModel {
    private String name;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object)
    {
        boolean sameSame = false;
        if (object != null && object instanceof DeviceModel)
        {
            sameSame = this.id.equalsIgnoreCase(((DeviceModel) object).id);
        }
        return sameSame;
    }
}
