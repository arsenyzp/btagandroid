package pro.devapp.btag.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.HashMap;
import java.util.UUID;

import pro.devapp.btag.MainActivity;
import pro.devapp.btag.R;
import pro.devapp.btag.components.Constants;

/**
 * Created by devapp on 01/12/2017.
 */

public class BLEService extends Service {

    public static final String TAG = BLEService.class.toString();
    private BackgroundBluetoothLEBinder myBinder = new BackgroundBluetoothLEBinder();
    public static final int FOREGROUND_ID = 1664;
    private HashMap<String, BluetoothGatt> bluetoothGatt = new HashMap<>();
    private HashMap<String, BluetoothGattService> linkLossServices = new HashMap<>();
    private HashMap<String, BluetoothGattService> immediateAlertServices = new HashMap<>();

    private class CustomBluetoothGattCallback extends BluetoothGattCallback {

        public static final String TAG = "BLE_CAllback";
        private final String address;

        private BluetoothGattCharacteristic batteryCharacteristic;

        private Runnable r;

        private Handler handler = new Handler();

        private Runnable trackRemoteRssi = null;

        public CustomBluetoothGattCallback(final String address) {
            this.address = address;
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(TAG, "onConnectionStateChange() address: " + address + " status => " + status + " newState => " + newState);
            if (BluetoothGatt.GATT_SUCCESS == status) {
                Log.d(TAG, "onConnectionStateChange() address: " + address + " newState => " + newState);
                if (newState == BluetoothProfile.STATE_CONNECTED) {
//                broadcaster.sendBroadcast(new Intent(GATT_CONNECTED));
                    gatt.discoverServices();
                }
                if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    gatt.close();
                }
            }

//        final boolean actionOnPowerOff = Preferences.isActionOnPowerOff(BluetoothLEService.this, this.address);
//        if (actionOnPowerOff || status == 8) {
//            Log.d(TAG, "onConnectionStateChange() address: " + address + " newState => " + newState);
//            if (newState == BluetoothProfile.STATE_DISCONNECTED) {
//                for (String action : Preferences.getActionOutOfBand(getApplicationContext(), this.address)) {
//                    sendAction(Preferences.Source.out_of_range, action);
//                }
//                enablePeerDeviceNotifyMe(gatt, false);
//            }
//        }
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            Log.d(TAG, "onReadRemoteRssi() address: " + address + " status => " + status + " rssi => " + rssi);
//        final Intent rssiIntent = new Intent(RSSI_RECEIVED);
//        rssiIntent.putExtra(RSSI_RECEIVED, rssi);
//        broadcaster.sendBroadcast(rssiIntent);
        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
            Log.d(TAG, "onServicesDiscovered() address: " + address + " status => " + status);

            launchTrackingRemoteRssi(gatt);

            //broadcaster.sendBroadcast(new Intent(SERVICES_DISCOVERED));
            if (BluetoothGatt.GATT_SUCCESS == status) {

//            for (String action : Preferences.getActionConnected(getApplicationContext(), this.address)) {
//                sendAction(Preferences.Source.connected, action);
//            }

                for (BluetoothGattService service : gatt.getServices()) {

                    Log.d(TAG, "service discovered: " + service.getUuid());

                    if (Constants.IMMEDIATE_ALERT_SERVICE.equals(service.getUuid())) {
                        if(!immediateAlertServices.containsKey(address)){
                            immediateAlertServices.put(address, service);
                        }
                        //broadcaster.sendBroadcast(new Intent(Constants.IMMEDIATE_ALERT_AVAILABLE));
                        gatt.readCharacteristic(getCharacteristic(gatt, Constants.IMMEDIATE_ALERT_SERVICE, Constants.ALERT_LEVEL_CHARACTERISTIC));
                        setCharacteristicNotification(gatt, service.getCharacteristics().get(0), true);

//                        gatt.readCharacteristic(service.getCharacteristics().get(0));
                    }

                    if (Constants.BATTERY_SERVICE.equals(service.getUuid())) {
                        batteryCharacteristic = service.getCharacteristics().get(0);
                        gatt.readCharacteristic(batteryCharacteristic);
                    }

                    if (Constants.FIND_ME_SERVICE.equals(service.getUuid())) {
                        if (!service.getCharacteristics().isEmpty()) {
                            BluetoothGattCharacteristic buttonCharacteristic = service.getCharacteristics().get(0);
                            setCharacteristicNotification(gatt, buttonCharacteristic, true);
                        }
                    }

                    if (Constants.LINK_LOSS_SERVICE.equals(service.getUuid())) {
                        if(!linkLossServices.containsKey(address)){
                            linkLossServices.put(address, service);
                        }
                    }
                }
                enablePeerDeviceNotifyMe(gatt, true);
            }
        }



        private void launchTrackingRemoteRssi(final BluetoothGatt gatt) {
            if (trackRemoteRssi != null) {
                handler.removeCallbacks(trackRemoteRssi);
            }
            trackRemoteRssi = new Runnable() {
                @Override
                public void run() {
                    gatt.readRemoteRssi();
                    handler.postDelayed(this, Constants.TRACK_REMOTE_RSSI_DELAY_MILLIS);
                }
            };
            handler.post(trackRemoteRssi);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Log.d(TAG, "onDescriptorWrite() " + descriptor.getCharacteristic().getUuid());
            if(batteryCharacteristic!=null){
                gatt.readCharacteristic(batteryCharacteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.d(TAG, "onCharacteristicChanged()");
            //final long delayDoubleClick = Preferences.getDoubleButtonDelay(getApplicationContext());

        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.d(TAG, "onCharacteristicRead()");
            if (characteristic.getValue() != null && characteristic.getValue().length > 0) {
                Log.d(TAG, "BATTERY_LEVEL 1 " + characteristic.getValue()[0]);
            }
            if (characteristic.getValue() != null && characteristic.getValue().length > 1) {
                Log.d(TAG, "BATTERY_LEVEL 2 " + characteristic.getValue()[1]);
            }
        }
    }

    private void setCharacteristicNotification(BluetoothGatt bluetoothgatt, BluetoothGattCharacteristic bluetoothgattcharacteristic, boolean flag) {
        bluetoothgatt.setCharacteristicNotification(bluetoothgattcharacteristic, flag);
        BluetoothGattDescriptor descriptor = bluetoothgattcharacteristic.getDescriptor(Constants.CLIENT_CHARACTERISTIC_CONFIG);
        if (descriptor != null) {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE); ???
            bluetoothgatt.writeDescriptor(descriptor);
        }
    }

    private BluetoothGattCharacteristic getCharacteristic(BluetoothGatt bluetoothgatt, UUID serviceUuid, UUID characteristicUuid) {
        if (bluetoothgatt != null) {
            BluetoothGattService service = bluetoothgatt.getService(serviceUuid);
            if (service != null) {
                return service.getCharacteristic(characteristicUuid);
            }
        }
        return null;
    }

    public void immediateAlert(String address, int alertType) {
        Log.d(TAG, "immediateAlert() - the device " + address);
        if (bluetoothGatt.get(address) == null || immediateAlertServices.get(address) == null || immediateAlertServices.get(address).getCharacteristics() == null || immediateAlertServices.get(address).getCharacteristics().size() == 0) {
//            somethingGoesWrong();
            return;
        }
        final BluetoothGattCharacteristic characteristic = immediateAlertServices.get(address).getCharacteristics().get(0);
        characteristic.setValue(alertType, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        bluetoothGatt.get(address).writeCharacteristic(characteristic);
        // Events.insert(getApplicationContext(), "immediate_alert", address, "" + alertType);
    }

    public void enablePeerDeviceNotifyMe(BluetoothGatt bluetoothgatt, boolean flag) {
        BluetoothGattCharacteristic bluetoothgattcharacteristic = getCharacteristic(bluetoothgatt, Constants.FIND_ME_SERVICE, Constants.FIND_ME_CHARACTERISTIC);
        if (bluetoothgattcharacteristic != null && (bluetoothgattcharacteristic.getProperties() | 0x10) > 0) {
            setCharacteristicNotification(bluetoothgatt, bluetoothgattcharacteristic, flag);
        }
    }


    public void readImmediateAlert(String address){
        if (bluetoothGatt.get(address) == null || immediateAlertServices.get(address) == null || immediateAlertServices.get(address).getCharacteristics() == null || immediateAlertServices.get(address).getCharacteristics().size() == 0) {
//            somethingGoesWrong();
            return;
        }
        bluetoothGatt.get(address).readCharacteristic(immediateAlertServices.get(address).getCharacteristics().get(0));
    }

    public void readEnablePeerDeviceNotifyMe(String address){
        if (bluetoothGatt.get(address) == null || linkLossServices.get(address) == null || linkLossServices.get(address).getCharacteristics() == null || linkLossServices.get(address).getCharacteristics().size() == 0) {
            //somethingGoesWrong();
            return;
        }
        bluetoothGatt.get(address).readCharacteristic(linkLossServices.get(address).getCharacteristics().get(0));
    }

    public void setLinkLossNotificationLevel(String address, int alertType) {
        Log.d(TAG, "setLinkLossNotificationLevel() - the device " + address);
        if (bluetoothGatt.get(address) == null || linkLossServices.get(address) == null || linkLossServices.get(address).getCharacteristics() == null || linkLossServices.get(address).getCharacteristics().size() == 0) {
            //somethingGoesWrong();
            return;
        }
        final BluetoothGattCharacteristic characteristic = linkLossServices.get(address).getCharacteristics().get(0);
        characteristic.setValue(alertType, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        bluetoothGatt.get(address).writeCharacteristic(characteristic);
    }



    public class BackgroundBluetoothLEBinder extends Binder {
        public BLEService service() {
            return BLEService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");
        //broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        setForegroundEnabled(true);
        connect();
        return START_STICKY;
    }

    public void setForegroundEnabled(boolean enabled) {
        if (enabled) {
            final Notification notification = new Notification.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getText(R.string.app_name))
                    .setTicker("Сервис в фоне")
                    .setContentText("BLE сервис работает")
                    .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0))
                    .setShowWhen(false).build();
            startForeground(FOREGROUND_ID, notification);
        } else {
            stopForeground(true);
        }
    }

    public synchronized void connect() {
        // выбираем список сохраненных устройств
//        final Cursor cursor = Devices.findDevices(this);
//        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
//            do {
//                final String address = cursor.getString(0);
//                if (Devices.isEnabled(this, address)) {
//                    this.connect(address);
//                }
//            } while (cursor.moveToNext());
//        }
    }

    public synchronized void connect(final String address) {
        if (!bluetoothGatt.containsKey(address) || bluetoothGatt.get(address) == null) {
            Log.d(TAG, "connect() - (new link) to device " + address);
            BluetoothDevice mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
            bluetoothGatt.put(address, mDevice.connectGatt(this, true, new CustomBluetoothGattCallback(address)));
        } else {
            Log.d(TAG, "connect() - discovering services for " + address);
            bluetoothGatt.get(address).discoverServices();
        }
    }

    public synchronized void disconnect(final String address) {
        if (bluetoothGatt.containsKey(address)) {
            Log.d(TAG, "disconnect() - to device " + address);
//            if (!Devices.isEnabled(this, address)) {
//                Log.d(TAG, "disconnect() - no background linked for " + address);
//                if (bluetoothGatt.get(address) != null) {
//                    bluetoothGatt.get(address).disconnect();
//                }
//                bluetoothGatt.remove(address);
//            }
            Log.d(TAG, "disconnect() - no background linked for " + address);
            if (bluetoothGatt.get(address) != null) {
                bluetoothGatt.get(address).disconnect();
            }
            bluetoothGatt.remove(address);
        }
    }

    public void immediateAlertTest(final String address){

    }

    public synchronized void remove(final String address) {
        if (bluetoothGatt.containsKey(address)) {
            Log.d(TAG, "remove() - to device " + address);
            if (bluetoothGatt.get(address) != null) {
                bluetoothGatt.get(address).disconnect();
            }
            bluetoothGatt.remove(address);
        }
    }

}
